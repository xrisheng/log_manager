import time
import re
import os.path
import queue
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
from datetime import datetime
from configs import *


class LogManager:
    def __init__(self):
        self.__queue = queue.Queue(MAX_QUEUE_LEN)  # a FIFO queue as buffer between log monitor and email pusher

    def __log_generator(self, log_path):  # use generator to reduce the memory usage
        log_file = open(log_path, 'r')  # open file
        # log_file.seek(0, 2)  # optional: start from the end of file and ignore the existing logs.

        while True:  # infinite loop of monitoring
            line = log_file.readline()
            if (not line) or (line == '\n'):  # if no new line or blank line, sleep
                time.sleep(MIN_READ_INTERVAL)
                continue
            yield line

    def log_monitor(self, log_config):  # log monitoring function
        while not os.path.isfile(log_config['path']):  # if log file not found, wait until it is created
            time.sleep(MIN_CHECK_INTERVAL)

        reg_obj = re.compile(log_config["reg_exp"])  # compile regular expression to reg object
        log_lines = self.__log_generator(log_config['path'])  # log_lines is the generator object

        print('thread ' + log_config['path'] + ' starts')

        for line in log_lines:
            line.rstrip('\n')  # delete trailing newline
            match = reg_obj.match(line)  # alternative: reg_obj.search(line)
            if (match is not None) == log_config['trigger']:
                st = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')  # create time stamp
                msg = str(st+'    '+log_config['path']+'    '+line)  # concat time stamp with log
                self.__queue.put(msg)  # push to queue

    def log_pusher(self):  # push log as email to user
        print('thread log pusher starts')

        while True:
            body = ''

            while not self.__queue.empty():  # get all contents in buffer
                body += (self.__queue.get())
                body += '\n'

            if body:  # if body is not empty, construct the message structure
                print(body)
                msg = MIMEMultipart()
                msg['From'] = email_configs['from']
                msg['To'] = ','.join(email_configs['to'])  # combine receivers list to a string joined by ','
                msg['Subject'] = "Log Manager Info"
                msg.attach(MIMEText(body, 'plain'))

                try:
                    server = smtplib.SMTP(email_configs['smtp'], email_configs['port'])
                    server.ehlo()
                    server.starttls()
                    server.login(email_configs['user'], email_configs['pwd'])

                    server.sendmail(msg['From'], msg['To'], msg.as_string())

                    server.close()
                    print('successfully sent the mail')

                except smtplib.SMTPException:
                    print('failed to send mail')
            time.sleep(MIN_MAIL_INTERVAL)

    def start(self):
        t_pool = ThreadPoolExecutor(max_workers=8)  # for every log file create a thread for monitoring
        t_pool.map(self.log_monitor, log_configs, chunksize=3)
        t_pusher = Thread(target=self.log_pusher, args=())  # create thread for email sending
        t_pusher.daemon = True
        t_pusher.start()


if __name__ == '__main__':
    manager = LogManager()
    manager.start()
