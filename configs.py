
log_configs = [{'path': 'log1.txt',  # The path of the log file to be monitored
                'reg_exp': '[Error|Warning]:*',  # The regular expression to be used in validation for EVERY SINGLE LINE of log
                'trigger': True},  # True:  once the content match to regular expression, send an email
                                    # False: once the content NOT match to regular expression, send an email

               {'path': 'log2.txt',
                'reg_exp': 'Error:*',
                'trigger': True}]

# To use the email service, you may need:
# 1. register an Gmail account WITHOUT 2-steps verification.
# 2. visit Google account, go to My account --> Sign-in & security -
# ->Apps with account access --> Allow less secure apps: ON

email_configs = {'from': 'XXXXXXX@gmail.com',   # the email sender, adjust to your own
                 'to': ['XXXXXXX@gmail.com'],   # the email receivers, THIS IS A LIST! adjust to your own
                 'user': 'XXXXXXX@gmail.com',   # your email account, adjust to your own
                 'pwd': 'XXXXXXX',              # password of your email account
                 'smtp': 'smtp.gmail.com',  # smtp server of gmail
                 'port': 587}               # another option is port 465,used by smtplib.SMTP_SSL("smtp.gmail.com", 465)

MIN_CHECK_INTERVAL = 30  # the minimum interval to check if log file exist, unit: second
MIN_READ_INTERVAL = 0.5  # the minimum interval to read if log file updated, unit: second
MIN_MAIL_INTERVAL = 10  # the minimum interval to write an email, unit: second
MAX_QUEUE_LEN = 100  # the email contains maximum last 100 lines of log
