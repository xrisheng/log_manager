from setuptools import setup
from setuptools import find_packages

setup(
    name='Log Manager',
    version='0.0.1',
    packages=find_packages(),
    description='General log file monitoring and pushing',
    author='Risheng Xu',
    license='MIT',
    author_email='xrisheng@gmail.com',
    classifiers=[
          'Programming Language :: Python :: 3.6',
    ],
)