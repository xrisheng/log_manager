# test environment:
# system: MacOS Sierra 10.12.6
# python: 3.6.3
# email: gmail without 2-steps verification
# test date: 07.01.2018

from log_manager import *
from configs import *


def dummy_logger():
    print('start dummy log writing')
    idx = 0
    while True:
        idx += 1
        for config in log_configs:
            file = open(config['path'], 'a')  # open file with write mode
            err_str = str('Error: No.' + str(idx) + '\n')
            war_str = str('Warning: No.' + str(idx) + '\n')
            file.write(err_str)
            file.write(war_str)
            file.close()
        time.sleep(20)


if __name__ == '__main__':
    t_logger = Thread(target=dummy_logger, args=())  # create thread for dummy logger
    t_logger.daemon = True
    t_logger.start()

    manager = LogManager()
    t_manager = Thread(target=manager.start, args=())  # create thread for dummy logger
    t_manager.start()




